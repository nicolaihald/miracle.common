﻿using System;
using System.Diagnostics;

namespace Miracle.Common.Benchmarks
{
    /// <summary>
    /// Light-weight class to provide benchmarking support to measure time used to run code.
    /// </summary>
    /// <remarks> Reference: http://commonlibrarynet.codeplex.com/ </remarks>
    /// <example>
    ///     <code>
    ///         Benchmark.Run(() => Console.WriteLine("Get benchmark using run method."));
    ///     </code>
    /// 
    /// nyew comment
    /// </example>
    public class Benchmark
    {
        private static readonly BenchmarkService Service = new BenchmarkService();


        /// <summary>
        /// Run a simple benchmark with the supplied action and get the result.
        /// </summary>
        /// <param name="action">The action to benchmark</param>
        public static BenchmarkResult Run(Action action)
        {
            return Report(action.Method.Name, string.Empty, null, action);
        }


        /// <summary>
        /// Run a simple benchmark with the supplied action and get the result.
        /// </summary>
        /// <param name="name">The name of the action to benchmark.</param>
        /// <param name="action">The action to benchmark</param>
        public static BenchmarkResult Run(string name, Action action)
        {
            return Report(name, string.Empty, null, action);
        }


        /// <summary>
        /// Run a simple benchmark with the supplied action and get the result.
        /// </summary>
        /// <param name="name">The name of the action to benchmark</param>
        /// <param name="message">A message representing the action to run benchmark against.</param>
        /// <param name="action"></param>
        public static BenchmarkResult Run(string name, string message, Action action)
        {
            return Report(name, message, null, action);
        }


        /// <summary>
        /// Run a simple benchmark with the supplied action and get the result and call the logger action supplied.
        /// </summary>
        /// <param name="name">The name of the action to benchmark</param>
        /// <param name="message">A message representing the action to run benchmark against.</param>
        /// <param name="action"></param>
        /// <param name="logger">The callback method for logging purposes.</param>
        public static BenchmarkResult Run(string name, string message, Action action, Action<BenchmarkResult> logger)
        {
            return Report(name, message, logger, action);
        }

        /// <summary>
        /// Run a simple benchmark with the supplied action and call the logger action supplied.
        /// </summary>
        /// <param name="name">The name of the action to benchmark</param>
        /// <param name="message">A message associated w/ the action to benchmark</param>
        /// <param name="logger">The callback method for logging purposes.</param>
        /// <param name="action">The action to benchmark.</param>
        public static BenchmarkResult Report(string name, string message, Action<BenchmarkResult> logger, Action action)
        {
            BenchmarkService service = Service ?? new BenchmarkService();
            return service.Report(name, message, logger, action);                
        }


        /// <summary>
        /// Run a simple benchmark with the supplied action and call the logger action supplied.
        /// </summary>
        /// <param name="name">The name of the action to benchmark</param>
        /// <param name="message">A message associated w/ the action to benchmark</param>
        /// <param name="logger">The callback method for logging purposes.</param>
        /// <param name="action">The action to benchmark.</param>
        public static void Get(string name, string message, Action<BenchmarkResult> logger, Action<BenchmarkService> action)
        {
            var service = new BenchmarkService(name, message, logger);
            action(service);
        }


        /// <summary>
        /// Measures the execution time for the specified <paramref name="action"/>. 
        /// (lightweight - very little overhead). 
        /// For full benchmark result, please use <see cref="Run(System.Action)"/> instead.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <returns></returns>
        public static TimeSpan Measure(Action action)
        {
            var watch = Stopwatch.StartNew();
            action();
            watch.Stop();
            return watch.Elapsed;
        }

    }
}