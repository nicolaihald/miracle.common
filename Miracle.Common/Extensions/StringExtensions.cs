﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Miracle.Common
{
    /// <summary>
    /// 
    /// </summary>
    public static class StringExtensions
    {
        #region --- Formatting: ---

        /// <summary>
        /// 	Formats the value with the parameters using string.Format.
        /// </summary>
        /// <param name = "value">The input string.</param>
        /// <param name = "parameters">The parameters.</param>
        /// <returns></returns>
        public static string FormatWith(this string value, params object[] parameters)
        {
            return String.Format(value, parameters);
        }

        /// <summary>
        /// Uppercase First Letter
        /// </summary>
        /// <param name = "value">The string value to process</param>
        public static string ToUpperFirstLetter(this string value)
        {
            if (value.IsEmptyOrWhiteSpace()) return String.Empty;

            char[] valueChars = value.ToCharArray();
            valueChars[0] = Char.ToUpper(valueChars[0]);

            return new string(valueChars);
        }
        
        /// <summary>Convert text's case to a title case</summary>
        /// <remarks>
        /// UppperCase characters is the source string after the first of each word are lowered, 
        /// unless the word is exactly 2 characters
        /// </remarks>
        public static string ToTitleCase(this string value)
        {
            return ToTitleCase(value, ExtensionMethodSetting.DefaultCulture);
        }

        /// <summary>Convert text's case to a title case</summary>
        /// <remarks>UppperCase characters is the source string after the first of each word are lowered, unless the word is exactly 2 characters</remarks>
        public static string ToTitleCase(this string value, CultureInfo culture)
        {
            return culture.TextInfo.ToTitleCase(value);
        }

        /// <summary>
        /// 	Extracts all digits from a string.
        /// </summary>
        /// <param name = "value">String containing digits to extract</param>
        /// <returns>
        /// 	All digits contained within the input string
        /// </returns>
        public static string ExtractDigits(this string value)
        {
            return value.Where(Char.IsDigit).Aggregate(new StringBuilder(value.Length), (sb, c) => sb.Append(c)).ToString();
        }
        #endregion

        #region --- Validation: ---

        /// <summary>
        /// true, if is valid email address
        /// </summary>
        /// <remarks>
        /// Reference: http://www.davidhayden.com/blog/dave/archive/2006/11/30/ExtensionMethodsCSharp.aspx
        /// </remarks>
        /// <param name="s">email address to test</param>
        /// <returns>true, if the email address is valid</returns>
        public static bool IsValidEmailAddress(this string s)
        {
            return new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,6}$").IsMatch(s);
        }

        /// <summary>
        /// Checks if url is valid (using a regular expression) 
        /// </summary>
        /// <remarks>
        /// Reference: complete (not only http) url regex can be found 
        /// at http://internet.ls-la.net/folklore/url-regexpr.html
        /// </remarks>
        /// <param name="url"></param>
        /// <returns>true, if the url is valid</returns>
        public static bool IsValidUrl(this string url)
        {
            const string strRegex = "^(https?://)" 
                                    + "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" //user@
                                    + @"(([0-9]{1,3}\.){3}[0-9]{1,3}" // IP- 199.194.52.184
                                    + "|" // allows either IP or domain
                                    + @"([0-9a-z_!~*'()-]+\.)*" // tertiary domain(s)- www.
                                    + @"([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]" // second level domain
                                    + @"(\.[a-z]{2,6})?)" // first level domain- .com or .museum is optional
                                    + "(:[0-9]{1,5})?" // port number- :80
                                    + "((/?)|" // a slash isn't required if there is no file name
                                    + "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";

            return new Regex(strRegex).IsMatch(url);
        }


        /// <summary>
        /// 	Tests whether the contents of a string is a numeric value
        /// </summary>
        /// <param name = "value">String to check</param>
        /// <returns>
        /// 	Boolean indicating whether or not the string contents are numeric
        /// </returns>
        public static bool IsNumeric(this string value)
        {
            float output;
            return Single.TryParse(value, out output);
        }


        /// <summary>
        /// 	Determines whether the specified string is null or empty.
        /// </summary>
        /// <param name = "value">The string value to check.</param>
        public static bool IsEmpty(this string value)
        {
            return ((value == null) || (value.Length == 0));
        }

        /// <summary>
        /// 	Determines whether the specified string is not null or empty.
        /// </summary>
        /// <param name = "value">The string value to check.</param>
        public static bool IsNotEmpty(this string value)
        {
            return (value.IsEmpty() == false);
        }

        /// <summary>
        /// 	Checks whether the string is empty and returns a default value in case.
        /// </summary>
        /// <param name = "value">The string to check.</param>
        /// <param name = "defaultValue">The default value.</param>
        /// <returns>Either the string or the default value.</returns>
        public static string IfEmpty(this string value, string defaultValue)
        {
            return (value.IsNotEmpty() ? value : defaultValue);
        }

        /// <summary>
        /// Finds out if the specified string contains null, empty or consists only of white-space characters
        /// </summary>
        /// <param name = "value">The input string</param>
        public static bool IsEmptyOrWhiteSpace(this string value)
        {
            return (value.IsEmpty() || value.All(t => Char.IsWhiteSpace(t)));
        }

        /// <summary>
        /// Determines whether the specified string is not null, empty or consists only of white-space characters
        /// </summary>
        /// <param name = "value">The string value to check</param>
        public static bool IsNotEmptyOrWhiteSpace(this string value)
        {
            return (value.IsEmptyOrWhiteSpace() == false);
        }

        /// <summary>
        /// Checks whether the string is null, empty or consists only of white-space characters 
        /// and returns a default value in case
        /// </summary>
        /// <param name = "value">The string to check</param>
        /// <param name = "defaultValue">The default value</param>
        /// <returns>Either the string or the default value</returns>
        public static string IfEmptyOrWhiteSpace(this string value, string defaultValue)
        {
            return (value.IsEmptyOrWhiteSpace() ? defaultValue : value);
        }


        /// <summary>
        /// 	Determines whether the comparison value strig is contained within the input value string
        /// </summary>
        /// <param name = "inputValue">The input value.</param>
        /// <param name = "comparisonValue">The comparison value.</param>
        /// <param name = "comparisonType">Type of the comparison to allow case sensitive or insensitive comparison.</param>
        /// <returns>
        /// 	<c>true</c> if input value contains the specified value, otherwise, <c>false</c>.
        /// </returns>
        public static bool Contains(this string inputValue, string comparisonValue, StringComparison comparisonType)
        {
            return (inputValue.IndexOf(comparisonValue, comparisonType) != -1);
        }
        #endregion 

        #region --- Appending: ---
        /// <summary>
        /// Multiply a string N number of times.
        /// </summary>
        /// <param name="str">String to multiply.</param>
        /// <param name="times">Number of times to multiply the string.</param>
        /// <returns>Original string multiplied N times.</returns>
        public static string Times(this string str, int times)
        {
            if (string.IsNullOrEmpty(str)) return string.Empty;
            if (times <= 1) return str;

            string strfinal = string.Empty;
            for (int ndx = 0; ndx < times; ndx++)
                strfinal += str;

            return strfinal;
        }

        /// <summary>
        /// Increases the string to the maximum length specified.
        /// If the string is already greater than maxlength, it is truncated if the flag truncate is true.
        /// </summary>
        /// <param name="str">The STR.</param>
        /// <param name="maxLength">Length of the max.</param>
        /// <param name="truncate">if set to <c>true</c> [truncate].</param>
        /// <returns>Increased string.</returns>
        public static string IncreaseTo(this string str, int maxLength, bool truncate)
        {
            if (string.IsNullOrEmpty(str)) return str;
            if (str.Length == maxLength) return str;
            if (str.Length > maxLength && truncate) return str.Truncate(maxLength);

            string original = str;

            while (str.Length < maxLength)
            {
                // Still less after appending by original string.
                if (str.Length + original.Length < maxLength)
                {
                    str += original;
                }
                else // Append partial.
                {
                    str += str.Substring(0, maxLength - str.Length);
                }
            }
            return str;
        }

        /// <summary>
        /// Increases the string to the maximum length specified.
        /// If the string is already greater than maxlength, it is truncated if the flag truncate is true.
        /// </summary>
        /// <param name="str">The STR.</param>
        /// <param name="minLength">String minimum length.</param>
        /// <param name="maxLength">Length of the max.</param>
        /// <param name="truncate">if set to <c>true</c> [truncate].</param>
        /// <returns>Randomly increased string.</returns>
        public static string IncreaseRandomly(this string str, int minLength, int maxLength, bool truncate)
        {
            Random random = new Random(minLength);
            int randomMaxLength = random.Next(minLength, maxLength);
            return IncreaseTo(str, randomMaxLength, truncate);
        }

        /// <summary>
        /// 	A generic version of System.String.Join()
        /// </summary>
        /// <typeparam name = "T">
        /// 	The type of the array to join
        /// </typeparam>
        /// <param name = "separator">
        /// 	The separator to appear between each element
        /// </param>
        /// <param name = "value">
        /// 	An array of values
        /// </param>
        /// <returns>
        /// 	The join.
        /// </returns>
        public static string Join<T>(string separator, T[] value)
        {
            if (value == null || value.Length == 0)
                return String.Empty;
            if (separator == null)
                separator = String.Empty;
            Converter<T, string> converter = o => o.ToString();
            return String.Join(separator, Array.ConvertAll(value, converter));
        }

        /// <summary>
        /// Concatenates the specified string value with the passed additional strings.
        /// </summary>
        /// <param name = "value">The original value.</param>
        /// <param name = "values">The additional string values to be concatenated.</param>
        /// <remarks>
        /// With string.concat, we must explicitly add the separator (in our case, space) between each string, 
        /// but it performs better than <see cref="string.Join(string,string[])"/>. 
        /// </remarks>
        /// <returns>The concatenated string.</returns>
        public static string ConcatWith(this string value, params string[] values)
        {
            return String.Concat(value, String.Concat(values));
        }


        #endregion

        #region --- Truncation: ---

        /// <summary>
        /// Truncate the text supplied by number of characters specified by <paramref name="maxChars"/>
        /// and then appends the (optional) suffix.
        /// </summary>
        /// <param name="txt">String to truncate.</param>
        /// <param name="maxChars">Maximum string length.</param>
        /// <param name="suffix">Suffix to append to string.</param>
        /// <returns>Truncated string with suffix.</returns>
        public static string Truncate(this string txt, int maxChars, string suffix = "")
        {
            if (string.IsNullOrEmpty(txt))
                return txt;

            if (txt.Length <= maxChars)
                return txt;

            // Now do the truncate and more.
            string partial = txt.Substring(0, maxChars);
            return partial + suffix;
        }

        /// <summary>
        /// 	Remove any instance of the given character from the current string.
        /// </summary>
        /// <param name = "value">
        /// 	The input.
        /// </param>
        /// <param name = "removeCharc">
        /// 	The remove char.
        /// </param>
        public static string Remove(this string value, params char[] removeCharc)
        {
            var result = value;
            if (!String.IsNullOrEmpty(result) && removeCharc != null)
                Array.ForEach(removeCharc, c => result = result.Remove(c.ToString(CultureInfo.InvariantCulture)));

            return result;

        }


        /// <summary>
        /// Remove any instance of the given string pattern from the current string.
        /// </summary>
        /// <param name="value">The input.</param>
        /// <param name="strings">The strings.</param>
        /// <returns></returns>
        public static string Remove(this string value, params string[] strings)
        {
            return strings.Aggregate(value, (current, c) => current.Replace(c, String.Empty));
        }


        /// <summary>
        /// 	Trims the text to a provided maximum length.
        /// </summary>
        /// <param name = "value">The input string.</param>
        /// <param name = "maxLength">Maximum length.</param>
        /// <returns></returns>
        public static string TrimToMaxLength(this string value, int maxLength)
        {
            return (value == null || value.Length <= maxLength ? value : value.Substring(0, maxLength));
        }

        /// <summary>
        /// 	Trims the text to a provided maximum length and adds a suffix if required.
        /// </summary>
        /// <param name = "value">The input string.</param>
        /// <param name = "maxLength">Maximum length.</param>
        /// <param name = "suffix">The suffix.</param>
        /// <returns></returns>
        public static string TrimToMaxLength(this string value, int maxLength, string suffix)
        {
            return (value == null || value.Length <= maxLength ? value : String.Concat(value.Substring(0, maxLength), suffix));
        }

        #endregion

        #region --- Conversion: ---
        /// <summary>
        /// Convert the text  to bytes.
        /// </summary>
        /// <param name="txt">Text to convert to bytes.</param>
        /// <returns>ASCII bytes representing the string.</returns>
        public static byte[] ToBytesAscii(this string txt)
        {
            return txt.ToBytesEncoding(new System.Text.ASCIIEncoding());
        }


        /// <summary>
        /// Convert the text to bytes using the system default code page.
        /// </summary>
        /// <param name="txt">Text to convert to bytes.</param>
        /// <returns>Bytes representing the string.</returns>
        public static byte[] ToBytes(this string txt)
        {
            return txt.ToBytesEncoding(System.Text.Encoding.Default);
        }


        /// <summary>
        /// Convert the text to bytes using a specified encoding.
        /// </summary>
        /// <param name="txt">Text to convert to bytes.</param>
        /// <param name="encoding">Encoding to use during the conversion.</param>
        /// <returns>Bytes representing the string.</returns>
        public static byte[] ToBytesEncoding(this string txt, Encoding encoding)
        {
            if (string.IsNullOrEmpty(txt))
                return new byte[] { };

            return encoding.GetBytes(txt);
        }


        /// <summary>
        /// Converts an ASCII byte array to a string.
        /// </summary>
        /// <param name="bytes">ASCII bytes.</param>
        /// <returns>String representation of ASCII bytes.</returns>
        public static string StringFromBytesASCII(this byte[] bytes)
        {
            return bytes.StringFromBytesEncoding(new System.Text.ASCIIEncoding());
        }


        /// <summary>
        /// Converts a byte array to a string using the system default code page.
        /// </summary>
        /// <param name="bytes">Byte array.</param>
        /// <returns>String representation of bytes.</returns>
        public static string StringFromBytes(this byte[] bytes)
        {
            return bytes.StringFromBytesEncoding(System.Text.Encoding.Default);
        }

        
        /// <summary>
        /// Converts a byte array to a string using a specified encoding.
        /// </summary>
        /// <param name="bytes">Byte array.</param>
        /// <param name="encoding">Encoding to use during the conversion.</param>
        /// <returns>String representation of bytes.</returns>
        public static string StringFromBytesEncoding(this byte[] bytes, Encoding encoding)
        {
            if (0 == bytes.GetLength(0))
                return null;

            return encoding.GetString(bytes);
        }


        /// <summary>
        /// Converts "yes/no/true/false/0/1"
        /// </summary>
        /// <param name="txt">String to convert to boolean.</param>
        /// <returns>Boolean converted from string.</returns>
        public static object ToBoolObject(this string txt)
        {
            return ToBool(txt) as object;
        }


        /// <summary>
        /// Converts "yes/no/true/false/0/1"
        /// </summary>
        /// <param name="txt">String to convert to boolean.</param>
        /// <returns>Boolean converted from string.</returns>
        public static bool ToBool(this string txt)
        {            
            if (string.IsNullOrEmpty(txt))
                return false;

            string trimed = txt.Trim().ToLower();
            if (trimed == "yes" || trimed == "true" || trimed == "1")
                return true;

            return false;
        }


        /// <summary>
        /// Converts a string to an integer and returns it as an object.
        /// </summary>
        /// <param name="txt">String to convert to integer.</param>
        /// <returns>Integer converted from string.</returns>
        /// <remarks>The method takes into starting monetary symbols like $.</remarks>
        public static object ToIntObject(this string txt)
        {
            return ToInt(txt) as object;
        }


        /// <summary>
        /// Converts a string to an integer.
        /// </summary>
        /// <param name="txt">String to convert to integer.</param>
        /// <returns>Integer converted from string.</returns>
        /// <remarks>The method takes into starting monetary symbols like $.</remarks>
        public static int ToInt(this string txt)
        {
            return ToNumber<int>(txt, (s) => Convert.ToInt32(Convert.ToDouble(s)), 0);
        }


        /// <summary>
        /// Converts a string to a long and returns it as an object.
        /// </summary>
        /// <param name="txt">String to convert to long.</param>
        /// <returns>Long converted from string.</returns>
        /// <remarks>The method takes into starting monetary symbols like $.</remarks>
        public static object ToLongObject(this string txt)
        {
            return ToLong(txt) as object;
        }


        /// <summary>
        /// Converts a string to a long.
        /// </summary>
        /// <param name="txt">String to convert to long.</param>
        /// <returns>Long converted from string.</returns>
        /// <remarks>The method takes into starting monetary symbols like $.</remarks>
        public static long ToLong(this string txt)
        {
            return ToNumber<long>(txt, (s) => Convert.ToInt64(s), 0);
        }


        /// <summary>
        /// Converts a string to a double and returns it as an object.
        /// </summary>
        /// <param name="txt">String to convert to double.</param>
        /// <returns>Double converted from string.</returns>
        /// <remarks>The method takes into starting monetary symbols like $.</remarks>
        public static object ToDoubleObject(this string txt)
        {
            return ToDouble(txt) as object;
        }


        /// <summary>
        /// Converts a string to a double.
        /// </summary>
        /// <param name="txt">String to convert from double.</param>
        /// <returns>Double converted from string.</returns>
        /// <remarks>The method takes into starting monetary symbols like $.</remarks>
        public static double ToDouble(this string txt)
        {
            return ToNumber<double>(txt, (s) => Convert.ToDouble(s), 0);
        }


        /// <summary>
        /// Converts a string to a float and returns it as an object.
        /// </summary>
        /// <param name="txt">String to convert to float.</param>
        /// <returns>Float converted from string.</returns>
        /// <remarks>The method takes into starting monetary symbols like $.</remarks>
        public static object ToFloatObject(this string txt)
        {
            return ToFloat(txt) as object;
        }


        /// <summary>
        /// Converts a string as a float and returns it.
        /// </summary>
        /// <param name="txt">String to convert to float.</param>
        /// <returns>Float converted from string.</returns>
        /// <remarks>The method takes into starting monetary symbols like $.</remarks>
        public static float ToFloat(this string txt)
        {
            return ToNumber<float>(txt, (s) => Convert.ToSingle(s), 0);
        }


        /// <summary>
        /// Converts to a number using the callback.
        /// </summary>
        /// <typeparam name="T">Type to convert to.</typeparam>
        /// <param name="txt">String to convert.</param>
        /// <param name="callback">Conversion callback method.</param>
        /// <param name="defaultValue">Default conversion value.</param>
        /// <returns>Instance of type converted from string.</returns>
        public static T ToNumber<T>(string txt, Func<string, T> callback, T defaultValue)
        {            
            if (string.IsNullOrEmpty(txt))
                return defaultValue;

            string trimed = txt.Trim().ToLower();
            // Parse $ or the system currency symbol.
            if (trimed.StartsWith("$") || trimed.StartsWith(Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencySymbol))
            {
                trimed = trimed.Substring(1);
            }
            return callback(trimed);
        }

        /// <summary>
        /// 	Convert the provided string to a Guid value.
        /// </summary>
        /// <param name = "value">The original string value.</param>
        /// <returns>The Guid</returns>
        public static Guid ToGuid(this string value)
        {
            return new Guid(value);
        }
        #endregion

        #region --- Replacement: ---
        /// <summary>
        /// Replaces the characters in the originalChars string with the
        /// corresponding characters of the newChars string.
        /// </summary>
        /// <param name="txt">String to operate on.</param>
        /// <param name="originalChars">String with original characters.</param>
        /// <param name="newChars">String with replacement characters.</param>
        /// <example>For an original string equal to "123456654321" and originalChars="35" and
        /// newChars "AB", the result will be "12A4B66B4A21".</example>
        /// <returns>String with replaced characters.</returns>
        public static string ReplaceChars(this string txt, string originalChars, string newChars)
        {
            string returned = "";

            for (int i = 0; i < txt.Length; i++)
            {
                int pos = originalChars.IndexOf(txt.Substring(i, 1), System.StringComparison.Ordinal);
                
                if (-1 != pos)
                    returned += newChars.Substring(pos, 1);
                else
                    returned += txt.Substring(i, 1);
            }
            return returned;
        }
        #endregion

        #region --- Lists: ---
        /// <summary>
        /// Prefixes all items in the list w/ the prefix value.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <param name="prefix">The prefix.</param>
        /// <returns>List with prefixes.</returns>
        public static List<string> PreFixWith(this List<string> items, string prefix)
        {
            for (int ndx = 0; ndx < items.Count; ndx++)
            {
                items[ndx] = prefix + items[ndx];
            }
            return items;
        }
        #endregion


        /// <summary>
        /// Used when we want to completely remove HTML code and not encode it with XML entities.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string StripHtml(this string input)
        {
            var tagsExpression = new Regex(@"</?.+?>");
            return tagsExpression.Replace(input, " ");
        }

    }
}




        


        


        


        


        


        


        


        
