﻿using System;
using System.Threading;
using Miracle.Common.Benchmarks;
using NUnit.Framework;
using ServiceStack.Text;

namespace Miracle.Common.Tests
{
    [TestFixture]
    public class BenchmarkTest
    {
        [Test]
        public void BenchmarkExampleTest()
        {
            //<doc:example>        
            // Example 1: Get a benchmark by running an action.
            BenchmarkResult result = Benchmark.Run(() => Console.WriteLine("Get benchmark using run method."));

            // Example 2: Get a benchmark by running a named action with a message.
            var result2 = Benchmark.Run("Example 2", "Running named benchmark", () => Console.WriteLine("Running example 2"));

            var result2a = Benchmark.Run("Example 2.a", "Running named benchmark", () => Console.WriteLine("Running example 2"));
            result2a.Duration.PrintDump();

            // Example 3: Run an action and report the benchmark data.
            Benchmark.Report("Example 3", "Reporting benchmark", (res) => Console.WriteLine(res.ToString()), () => Console.WriteLine("Running example 3"));
            Benchmark.Report("Example 3b", "Reporting benchmark", null, () => Console.WriteLine("Running example 3b"));

            // Example 4: Get instance of Benchmark service and run multiple reports on it.
            Benchmark.Get("Example 4", "testing", null, (bm) =>
                {
                    bm.Run("4a", "testing", () => Console.WriteLine("Running 4a"));
                    bm.Run("4b", "testing", () => Console.WriteLine("Running 4b"));
                    bm.Run("4c", "testing", () => Console.WriteLine("Running 4c"));
                });

            // Example 5: Get instace of benchmark service manually.
            var bm2 = new BenchmarkService("Example 5", "manually instance of bm service", null);
            bm2.Run(() => Console.WriteLine("Running example 5"));
        }

        [Test]
        public void BenchmarkElapsedTest()
        {
            Action action = () => Thread.Sleep(100);
            Benchmark.Run("test", action);
            var result = Benchmark.Run("test", action);
            Assert.That(result.TimeStarted + result.Duration, Is.EqualTo(result.TimeEnded));
        }


        [Test]
        public void BenchmarkMeasureTest()
        {
            Action action = () => Thread.Sleep(100);
            Benchmark.Measure(action).Milliseconds.PrintDump();

        }
    }

}