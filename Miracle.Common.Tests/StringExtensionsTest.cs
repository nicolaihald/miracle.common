﻿using NUnit.Framework;
using ServiceStack.Text;

namespace Miracle.Common.Tests
{

    [TestFixture]
    public class StringExtensionsTest
    {
        [Test]
        public void IsValidEmailAddressTest()
        {
            Assert.IsTrue("miracle@miracle.dk".IsValidEmailAddress());
            Assert.IsTrue("miracle69@email.miracle.co.uk".IsValidEmailAddress());
            Assert.IsFalse("adfasdf".IsValidEmailAddress());
            Assert.IsFalse("asd@asdf".IsValidEmailAddress());
        }

        /// <summary>
        ///A test for IsValidUrl
        ///</summary>
        [Test]
        public void IsValidUrlTest()
        {
            Assert.IsTrue("http://www.miracleas.dk".IsValidUrl());
            Assert.IsTrue("https://www.miracleas.dk/#some_anchor".IsValidUrl());
            Assert.IsTrue("https://localhost".IsValidUrl());
            Assert.IsTrue("http://www.abcde.nf.net/signs-banner.jpg".IsValidUrl());
            Assert.IsTrue("http://aa-bbbb.cc.bla.com:80800/test/test.aspx?dd=dd&id=dki".IsValidUrl());
            Assert.IsFalse("http:wwwmiracleasdk".IsValidUrl());
            Assert.IsFalse("http:wwwmiracleas.dk".IsValidUrl());
            Assert.IsFalse("http:www.miracleas.dk".IsValidUrl());
            Assert.IsFalse("http://www.code project.com".IsValidUrl());
        }

        [Test]
        public void TruncateTest()
        {
            const string input = "The quick brown fox jumps over the lazy dog";
            const int count = 9;
            const string endings = "...";
            const string expected = "The quick...";

            var actual = input.Truncate(count, endings);

            actual.DumpToConsole("actual");
            Assert.AreEqual(expected, actual);
        }

    }
}