﻿using System;

namespace Miracle.Common.Tests
{
    static class TestSpecificStringExtensions
    {
        public static string DumpToConsole(this string @string, string description = null)
        {
            Console.WriteLine("{0}{1}", (description.IsNotEmptyOrWhiteSpace() ? description + ": " : ""),  @string);
            return @string;

        }
    }
}